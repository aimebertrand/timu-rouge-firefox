#+TITLE: Firefox Rouge theme
#+AUTHOR: Aimé Bertrand
#+DATE: [2022-10-09 Sun]
#+LANGUAGE: en
#+OPTIONS: d:t toc:nil num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd-dark.css" />
#+KEYWORDS: firefox gtk gnome theme rouge dark
#+STARTUP: indent showall

* Description
Firefox Color theme inspired by the [[https://vscodethemes.com/e/josef.rouge-theme][Rouge Theme for VSCode]].

#+html: <p align="center"><img src="img/timu-rouge-screenshot.png" width="100%"/></p>

* Installation
** Installation script
1. Clone this repo and enter folder:

#+begin_src sh
  git clone https://gitlab.com/aimebertrand/timu-rouge-firefox.git && cd timu-rouge-firefox
#+end_src

3. Run installation script

*Install script*

#+begin_src sh
  ./scripts/install.sh # Standard
  ./scripts/install.sh -f ~/.var/app/org.mozilla.firefox/.mozilla/firefox # Flatpak
  ./scripts/install.sh -f ~/snap/firefox/common/.mozilla/firefox #Snap
#+end_src

** Manual installation
1.  Go to =about:support= in Firefox.
2.  Application Basics > Profile Directory > Open Directory.
3.  Open directory in a terminal.
4.  Create a =chrome= directory if it doesn't exist:

#+begin_src sh
  mkdir -p chrome
  cd chrome
#+end_src

5.  Clone this repo to a subdirectory:

#+begin_src sh
  git clone https://gitlab.com/aimebertrand/timu-rouge-firefox.git
#+end_src

6.  Create single-line user CSS files if non-existent or empty (at least one line is needed for =sed=):

#+begin_src sh
  [[ -s userChrome.css ]] || echo >> userChrome.css
  [[ -s userContent.css ]] || echo >> userContent.css
#+end_src

7.  Import this theme at the beginning of the CSS files (all =@import= must come before any existing =@namespace= declarations):

#+begin_src sh
  sed -i '1s/^/@import "timu-rouge-firefox\/userChrome.css";\n/' userChrome.css
  sed -i '1s/^/@import "timu-rouge-firefox\/userContent.css";\n/' userContent.css
#+end_src

8.  Symlink preferences file:

#+begin_src sh
  cd .. # Go back to the profile directory
  ln -fs chrome/timu-rouge-firefox/configuration/user.js user.js
#+end_src

9.  Restart Firefox.

10. Open Firefox customization panel and move the new tab button to headerbar.

11. Be happy with your new gnomish Firefox.

* Updating
You can follow the installation script steps again to update the theme.

* Uninstalling
1. Go to your profile folder. (Go to =about:support= in Firefox > Application Basics > Profile Directory > Open Directory)
2. Remove =chrome= folder.

* Enabling optional features
Optional features can be enabled by creating new =boolean= preferences in =about:config=.

1. Go to the =about:config= page
2. Type the key of the feature you want to enable
3. Set it as a =boolean= and click on the add button
4. Restart Firefox

** Features
- =gnomeTheme.hideSingleTab= :: Hide the tab bar when only one tab is open.

- =gnomeTheme.normalWidthTabs= :: Use normal width tabs as default Firefox.

- =gnomeTheme.bookmarksToolbarUnderTabs= :: Move Bookmarks toolbar under tabs.

- =gnomeTheme.activeTabContrast= :: Add more contrast to the active tab.

- =gnomeTheme.closeOnlySelectedTabs= :: Show the close button on the selected tab only.

- =gnomeTheme.systemIcons= :: Use system theme icons instead of Adwaita icons included by theme.

- =gnomeTheme.symbolicTabIcons= :: Make all tab icons look kinda like symbolic icons.

- =gnomeTheme.hideWebrtcIndicator= :: Hide redundant WebRTC indicator since GNOME provides their own privacy icons in the top right.

- =gnomeTheme.dragWindowHeaderbarButtons= :: Allow dragging the window from headerbar buttons.

* Credits
Used work by [[https://github.com/rafaelmardojai/firefox-gnome-theme][Rafael Mardojai CM]] and [[https://github.com/lunakurame/firefox-gnome-theme][Luna Kurame]] as boilerplate.
